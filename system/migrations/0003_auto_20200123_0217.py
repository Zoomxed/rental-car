# Generated by Django 3.0.2 on 2020-01-22 21:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('system', '0002_auto_20200123_0044'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='cell_no',
            field=models.CharField(max_length=15, verbose_name='Номер телефона'),
        ),
    ]
