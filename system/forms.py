from django import forms
from .models import Car, Order, PrivateMsg
from datetime import datetime


class CarForm(forms.ModelForm):
    class Meta:
        model = Car
        fields = [
            'image',
            'brand',
            'car_name',
            'company_name',
            'num_of_seats',
            'cost_par_day',
            'content',
        ]


class OrderForm(forms.ModelForm):
    date_from = forms.DateTimeField(input_formats=['%Y-%m-%d %H:%M'], label='Дата начала аренды', required=True)
    date_to = forms.DateTimeField(input_formats=['%Y-%m-%d %H:%M'], label='Дата окончания аренды', required=True)

    class Meta:
        model = Order
        fields = [
            'car_name',
            'employee_name',
            'cell_no',
            'address',
            'date_from',
            'date_to',
        ]


class MessageForm(forms.ModelForm):
    class Meta:
        model = PrivateMsg
        fields = [
            'name',
            'email',
            'message',
        ]
