from django.db import models
from django import forms


def uploaded_location(instance, filename):
    return '{0}/{1}'.format(instance.car_name, filename)


class Car(models.Model):
    image = models.ImageField(upload_to=uploaded_location, null=True, blank=True, width_field='width_field',
                              height_field='height_field', verbose_name='Фото машины')
    height_field = models.IntegerField(default=0)
    width_field = models.IntegerField(default=0)
    brand = models.CharField(verbose_name='Бренд', default='', max_length=50)
    car_name = models.CharField(verbose_name='Название машины', max_length=50)
    company_name = models.CharField(verbose_name='Имя компании', max_length=50)
    num_of_seats = models.IntegerField(verbose_name='Количество мест', default=5)
    cost_par_day = models.CharField(verbose_name='Стоимость за 1 день р.', max_length=50)
    content = models.TextField(verbose_name='Дополнительная информация')
    like = models.IntegerField(default=0)

    def __str__(self):
        return self.car_name

    def get_absolute_url(self):
        return '/car/{}/'.format(self.id)


class Order(models.Model):
    car_name = models.CharField(verbose_name='Название машины', max_length=50)
    employee_name = models.CharField(verbose_name='Бренд', max_length=100)
    cell_no = models.CharField(verbose_name='Номер телефона', max_length=15)
    address = models.TextField(verbose_name='Адрес')
    date_from = models.DateTimeField(verbose_name='Дата начала аренды')
    date_to = models.DateTimeField(verbose_name='Дата окончания аренды')

    def __str__(self):
        return self.car_name

    def get_absolute_url(self):
        return '/car/detail/{}/'.format(self.id)


class PrivateMsg(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField()
    message = models.TextField()
