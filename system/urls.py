from django.conf.urls import url
from django.urls import re_path
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from . import views
import system

urlpatterns = [
    re_path(r'^$', system.views.home, name='home'),

    re_path(r'^carlist/$', system.views.car_list, name='car_list'),
    re_path(r'^createOrder/$', system.views.order_created, name='order_create'),

    re_path(r'^(?P<id>\d+)/edit/$', system.views.car_update, name='car_edit'),


    re_path(r'^(?P<id>\d+)/$', system.views.car_detail, name='car_detail'),
    re_path(r'^detail/(?P<id>\d+)/$', system.views.order_detail, name='order_detail'),

    re_path(r'^(?P<id>\d+)/delete/$', system.views.car_delete, name='car_delete'),
    re_path(r'^(?P<id>\d+)/deleteOrder/$', system.views.order_delete, name='order_delete'),

    re_path(r'^contact/$', system.views.contact, name='contact'),

    re_path(r'^newcar/$', system.views.newcar, name='newcar'),
    re_path(r'^(?P<id>\d+)/like/$', system.views.like_update, name='like'),
    re_path(r'^popularcar/$', system.views.popular_car, name='popularcar'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
